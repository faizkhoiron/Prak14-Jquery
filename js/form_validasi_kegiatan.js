//buat fungsi
$(function() {
 //panggil nama form lalu gunakan fungsi validasi
 $("form[name='form_kegiatan']").validate({
     rules: {
         kode: {
             required:true,
             maxlength:10,
     },
     judul: "required",
     narasumber: "required",
     deskripsi: "required",
  },
 //buat aturan untuk input form
   messages: {
        kode: {
           required : "Kode wajib di isi !!",
           maxlength : "maksimum 10 character !!"
     },
       //tampilkan pesan
     judul: "Judul wajib diisi !!",
     narasumber: "Narasumber wajib diisi !!",
     deskripsi:"Deskripsi wajib diisi",
   },
  //submit form
           submitHandler: function(form) {
              form.submit();
          }
       });
  });
